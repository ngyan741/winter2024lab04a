public class Student{
	private String name;
	private int grade;
	private boolean isFirstYear;
	private String firstLanguage;
	private int amountLearnt;
	
	public Student(String name, int grade){
		this.name = name;
		this.grade = grade;
		this.isFirstYear = false;
		this.firstLanguage = "n/a";
		this.amountLearnt = 0;
	}
	
	public void setIsFirstYear(boolean newIsFirstYear){
		this.isFirstYear = newIsFirstYear;
	}
	public void setFirstLanguage(String newFirstLanguage){
		this.firstLanguage = newFirstLanguage;
	}
	
	public String getName(){
		return this.name;
	}
	public int getGrade(){
		return this.grade;
	}
	public boolean getIsFirstYear(){
		return this.isFirstYear;
	}
	public String getFirstLanguage(){
		return this.firstLanguage;
	}
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	
	public void introduceSelf(){
		if(this.firstLanguage.equals("Francais")){
			System.out.println("Salut! Je m'appelle " + this.name + ".");
			if(this.isFirstYear == true){
				System.out.println("C'est ma premiere annee a Dawson!");
			}
			else{
				System.out.println("Ce n'est pas ma premiere annee a Dawson.");
			}
			System.out.println("Ma langue maternelle est " + this.firstLanguage + ". Enchante!");
		}
		else{
			System.out.println("Hi! My name is " + this.name + ".");
			if(this.isFirstYear == true){
				System.out.println("It's my first year at Dawson!");
			}
			else{
				System.out.println("It's not my first year at Dawson.");
			}
			System.out.println("My first language is " + this.firstLanguage + ". Nice to meet you!");
		}
	}
	
	public void increaseGrade(){
		System.out.println("Current grade: " + this.grade);
		System.out.println("Currently studying..." + "\n" + "..." + "\n" + ".." + "\n" + ".");
		this.grade++;
		System.out.println("Studying complete! Grade: " + this.grade);
	}
	
	public void study(int amountStudied){
		this.amountLearnt += amountStudied;
	}
	
	public void checkFailing(){
		if(this.grade < 60){
			System.out.println("Hey " + this.name + ", you should start studying!");
		}
		else if(this.grade > 60 && this.grade < 70){
			System.out.println("Hey " + this.name + ", you might want to start studying!");
		}
		else{
			System.out.println("Hey " + this.name + ", you're doing great! Keep it up!");
		}
	}
}