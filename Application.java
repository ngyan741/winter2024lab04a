import java.util.Scanner;
public class Application{
	public static void main(String args[]){
		//Scanner reader = new Scanner(System.in);
		//System.out.println("Type a number!");
		//int amountStudied = Integer.parseInt(reader.nextLine());
		
		Student student1 = new Student("Timmy", 65);
	
		Student student2 = new Student("Jessie", 86);
		
		Student[] section4 = new Student[3];
		section4[0] = student1; 
		section4[1] = student2;
		
		section4[2] = new Student("Barry", 54);
		
		Student student4 = new Student("John", 95);
		student4.setIsFirstYear(true);
		
		System.out.println(student4.getName());
		System.out.println(student4.getGrade());
		System.out.println(student4.getIsFirstYear());
		System.out.println(student4.getFirstLanguage());
		System.out.println(student4.getAmountLearnt());
		
	}
}